class Solution:
    def twoSum(self, nums: list[int], target: int) -> list[list[int]]:
        stack = []
        rslt = []
        for n in nums:
            tmp = target - n
            if tmp in stack:
                rslt.append((tmp, n))
            stack.append(n)
        return rslt

    def fourSum(self, nums: list[int], target: int) -> list[list[int]]:
        nums.sort()
        nlen = len(nums)
        rslt = []
        for i in range(nlen - 3):
            for j in range(i + 1, nlen - 2):
                sums = self.twoSum(nums[j + 1 : nlen], target - nums[i] - nums[j])
                for s in sums:
                    tmp = (nums[i], nums[j]) + s
                    rslt.append(tmp)
        return set(rslt)
