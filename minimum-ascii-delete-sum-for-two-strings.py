from functools import cache


class Solution:
    @cache
    def _rec(self, idx1: int, idx2: int) -> int:
        if idx1 == len(self.s1) and idx2 == len(self.s2):
            return 0

        if idx1 == len(self.s1):
            return sum(map(ord, self.s2[idx2:]))

        if idx2 == len(self.s2):
            return sum(map(ord, self.s1[idx1:]))

        if self.s1[idx1] == self.s2[idx2]:
            return self._rec(idx1 + 1, idx2 + 1)

        delIdx1 = ord(self.s1[idx1]) + self._rec(idx1 + 1, idx2)
        delIdx2 = ord(self.s2[idx2]) + self._rec(idx1, idx2 + 1)
        return min(delIdx1, delIdx2)

    def minimumDeleteSum(self, s1: str, s2: str) -> int:
        self.s1 = s1
        self.s2 = s2
        return self._rec(0, 0)
