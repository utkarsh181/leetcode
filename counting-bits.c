int
ones (int n)
{
  int b;

  for (b = 0; n != 0; n >>= 1)
    if (n & 1)
      b++;
  return b;
}

int *
countBits (int n, int *rsize)
{
  int i, *rslt;

  *rsize = n + 1;
  rslt = (int *) calloc (n + 1, sizeof (int));
  for (i = 0; i <= n; i++)
    rslt[i] = ones (i);
  return rslt;
}
