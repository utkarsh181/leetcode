class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        profit = 0
        buyingPrice = prices[0]
        for sellingPrice in prices[1:]:
            if sellingPrice > buyingPrice:
                profit += sellingPrice - buyingPrice
            buyingPrice = sellingPrice
        return profit
