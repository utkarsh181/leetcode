# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> Optional[ListNode]:
        nA, nB = headA, headB
        cA, cB = 0, 0
        while nA != nB and cA < 2 and cB < 2:
            nA = nA.next
            nB = nB.next
            if not nA:
                nA = headB
                cA += 1
            if not nB:
                nB = headA
                cB += 1
        if cA == 2 or cB == 2:
            return None
        else:
            return nA
