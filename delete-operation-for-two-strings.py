class Solution:
    def minDistance(self, word1: str, word2: str) -> int:
        row = len(word1)
        col = len(word2)
        dp = [[0] * col for _ in range(row)]
        for i in range(row):
            for j in range(col):
                if word1[i] == word2[j]:
                    dp[i][j] = 1 if i - 1 < 0 or j - 1 < 0 else dp[i - 1][j - 1] + 1
                else:
                    left = 0 if i - 1 < 0 else dp[i - 1][j]
                    up = 0 if j - 1 < 0 else dp[i][j - 1]
                    dp[i][j] = max(left, up)
        if row == col:
            return (row - dp[row - 1][col - 1]) * 2
        else:
            return (max(row, col) - dp[row - 1][col - 1]
                    + min(row, col) - dp[row - 1][col - 1])
