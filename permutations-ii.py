class Solution:
    def _backtrack(self, nums: list[int]) -> list[list[int]]:
        if nums == []:
            return [[]]

        rslt = []
        for idx, num in enumerate(nums):
            if idx > 0 and (num == nums[0] or num == nums[idx - 1]):
                continue
            for p in self.permuteUnique(nums[:idx] + nums[idx + 1 :]):
                rslt.append([num] + p)
        return rslt
    
    def permuteUnique(self, nums: list[int]) -> list[list[int]]:
        return self._backtrack(sorted(nums))
