class Solution:
    def checkIfExist(self, arr: List[int]) -> bool:
        buf = set()
        for n in arr:
            tmp1 = n * 2
            tmp2 = n / 2
            if tmp1 in buf or tmp2 in buf:
                return True
            buf.add(n)
        return False
