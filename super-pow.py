# Using in-built pow

class Solution:
    def superPow(self, a: int, b: List[int]) -> int:
        if a == 1:
            return 1
        return pow(a, int(''.join(map(str, b))), 1337)

# By traversing to exponent list B.

class Solution:
    def superPow(self, a, b):
        result = 1
        for digit in b:
            result = pow(result, 10, 1337) * pow(a, digit, 1337) % 1337
        return result
