class Solution:
    def findErrorNums(self, nums: List[int]) -> List[int]:
        unique = {}
        for i in nums:
            if i not in unique:
                unique[i] = 1
            else:
                repeated = i
        for i in range(1, len(nums) + 1):
            if i not in unique:
                missing = i
                break
        return [repeated, missing]
