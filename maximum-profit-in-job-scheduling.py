class Solution:
    @cache
    def _rec(self, idx: int) -> int:
        if idx >= len(self.jobs):
            return 0

        endTime = self.jobs[idx][1]
        profit = self.jobs[idx][2]
        nextIdx = bisect.bisect(self.jobs, endTime, key=lambda j: j[0])
        return max(self._rec(idx + 1), profit + self._rec(nextIdx))

    def jobScheduling(
        self, startTime: List[int], endTime: List[int], profit: List[int]
    ) -> int:
        self.jobs = [job for job in zip(startTime, endTime, profit)]
        self.jobs.sort()
        return self._rec(0)
