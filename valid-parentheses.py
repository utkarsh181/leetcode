class Solution:
    def isValid(self, string: str) -> bool:
        parens = []
        pairs = {")": "(", "}": "{", "]": "["}

        for paren in string:
            if paren in ["(", "{", "["]:
                parens.append(paren)
            elif not parens:
                return False
            elif pairs[paren] != parens.pop():
                return False

        return not parens
