class Solution:
    def checkSubarraySum(self, nums: List[int], k: int) -> bool:
        if len(nums) < 2:
            return False

        total = 0
        multipleTotalHash = {0: -1}
        for idx, num in enumerate(nums):
            total += num
            tmp = total % k
            if tmp not in multipleTotalHash:
                multipleTotalHash[tmp] = idx
            elif idx - multipleTotalHash[tmp] > 1:
                return True
        return False

