class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:
        def closest(arr, item, left, right, max_index):
            if left > right:
                if right == -1:
                    return left
                elif left == max_index + 1:
                    return right
                elif abs(arr[left] - item) < abs(arr[right] - item):
                    return left
                else:
                    return right

            mid = left + (right - left) // 2
            if arr[mid] == item:
                return mid
            elif arr[mid] > item:
                return closest(arr, item, left, mid - 1, max_index)
            else:
                return closest(arr, item, mid + 1, right, max_index)

        rslt = []
        max_index = len(arr) - 1
        mid = closest(arr, x, 0, max_index, max_index)
        rslt.append(arr[mid])
        k -= 1

        left, right = mid - 1, mid + 1
        while k > 0 and left >= 0 and right <= max_index :
            tmp = abs(arr[left] - x) - abs(arr[right] - x)
            if tmp < 0 or (tmp == 0 and arr[left] < arr[right]):
                rslt.insert(0, arr[left])
                left -= 1
            else:
                rslt.append(arr[right])
                right += 1
            k -= 1
        while k > 0 and left >= 0:
            rslt.insert(0, arr[left])
            left -= 1
            k -= 1
        while k > 0 and right <= max_index:
            rslt.append(arr[right])
            right += 1
            k -= 1
        return rslt
