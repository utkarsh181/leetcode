# Your TimeMap object will be instantiated and called as such:
# obj = TimeMap()
# obj.set(key,value,timestamp)
# param_2 = obj.get(key,timestamp)
class TimeMap:
    def __init__(self):
        self.table = {}

    def set(self, key: str, value: str, timestamp: int) -> None:
        if key not in self.table:
            self.table[key] = {}
        self.table[key][timestamp] = value

    def get(self, key: str, timestamp: int) -> str:
        rslt = ''
        for t in range(timestamp, 0, -1):
            try:
                rslt = self.table[key][t]
                break
            except KeyError:
                continue
        return rslt
