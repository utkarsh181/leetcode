class Solution:
    def threeSumClosest(self, arr: List[int], x: int) -> int:
        arr.sort()
        n = len(arr)
        rslt = arr[0] + arr[1] + arr[2]
        for i in range(n - 1):
            j = i + 1
            k = n - 1
            while j < k:
                cur = arr[i] + arr[j] + arr[k]
                if abs(x - rslt) > abs(x - cur):
                    rslt = cur
                
                if cur <= x:
                    j += 1
                else:
                    k -= 1
        return rslt