from functools import cmp_to_key


class Solution:
    def _compare(self, num1: str, num2: str) -> int:
        left = num1 + num2
        right = num2 + num1

        if left > right:
            return 1
        elif left < right:
            return -1
        else:
            return 0

    def largestNumber(self, nums: list[int]) -> str:
        nums = list(map(str, nums))
        nums.sort(key=cmp_to_key(self._compare), reverse=True)

        if nums[0] == "0":
            return "0"

        return "".join(nums)
