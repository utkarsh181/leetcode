class Solution:
    def removeKdigits(self, num: str, k: int) -> str:
        if len(num) == k:
            return '0'
            
        digits = []
        for d in num:
            while digits and d < digits[-1] and k:
                digits.pop()
                k -= 1

            if digits or d != '0':
                digits.append(d)
        
        if k:
            digits = digits[:-k]

        if not digits:
            return '0'
        else:
            return ''.join(digits)
            