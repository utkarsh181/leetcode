class Solution:
    def maxLength(self, arr: List[str]) -> int:
        dp = [set()]
        for s1 in arr:
            tmp = set(s1)
            if len(tmp) != len(s1):
                continue
            for s2 in dp.copy():
                if tmp & s2:
                    continue
                dp.append(tmp | s2)
        return max(map(len, dp))