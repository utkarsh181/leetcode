# class Solution:
#     def maxValue(self, events: List[List[int]], k: int) -> int:
#         rslt = 0
#         events.sort()

#         for event in events[: -k + 1]:
#             end_day = event[1]
#             value = event[2]
#             count = 1

#             while True:
#                 idx = bisect.bisect(events, end_day, key=lambda e: e[0])
#                 count += 1
#                 if idx >= len(events) or count > k:
#                     break
#                 else:
#                     end_day = events[idx][1]
#                     value += events[idx][2]

#             rslt = max(rslt, value)

#         return rslt


class Solution:
    @cache
    def _rec(self, idx: int, k: int) -> int:
        if k == 0 or idx >= len(self.events):
            return 0

        end_day = self.events[idx][1]
        value = self.events[idx][2]
        next_idx = bisect.bisect(self.events, end_day, key=lambda e: e[0])
        return max(self._rec(idx + 1, k), value + self._rec(next_idx, k - 1))

    def maxValue(self, events: List[List[int]], k: int) -> int:
        self.events = sorted(events)
        return self._rec(0, k)
