class Solution:
    def findDuplicates(self, nums: List[int]) -> List[int]:
        rslt = []
        nums.sort()
        nlen = len(nums)
        for i in range(nlen - 1):
            if nums[i] == nums[i+1]:
                rslt.append(nums[i])
        return rslt