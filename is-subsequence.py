class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        rem_of_t = iter(t)
        return all(c in rem_of_t for c in s)
