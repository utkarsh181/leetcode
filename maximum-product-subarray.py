class Solution:
    def maxProduct(self, nums: list[int]) -> int:
        curMax = nums[0]
        curMin = nums[0]
        rslt = nums[0]
        for i in range(1, len(nums)):
            if nums[i] < 0:
                curMax, curMin = curMin, curMax
            curMax = max(nums[i], nums[i] * curMax)
            curMin = min(nums[i], nums[i] * curMin)
            rslt = max(rslt, curMax)
        return rslt
