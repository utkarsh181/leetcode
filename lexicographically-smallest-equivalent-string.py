class UnionFind:
    def __init__(self) -> None:
        self._parent = {}

    def find(self, x: any) -> any:
        if x not in self._parent:
            return x
        if self._parent[x] == x:
            return x

        return self.find(self._parent[x])

    def union(self, x: any, y: any) -> None:
        rootX = self.find(x)
        rootY = self.find(y)

        if rootX == rootY:
            return
        elif rootX < rootY:
            self._parent[rootY] = rootX
        else:
            self._parent[rootX] = rootY


class Solution:
    def smallestEquivalentString(self, s1: str, s2: str, baseStr: str) -> str:
        rslt = UnionFind()

        for x, y in zip(s1, s2):
            rslt.union(x, y)

        return "".join(rslt.find(x) for x in baseStr)
