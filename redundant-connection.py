class Solution:
    def already_connected(
        self,
        graph: Dict[int, List[int]],
        discovered: Dict[int, List[int]],
        start: int,
        end: int,
    ) -> bool:
        discovered[start] = True
        for adj in graph.get(start, []):
            if discovered.get(adj, False):
                continue
            if adj == end or self.already_connected(graph, discovered, adj, end):
                return True
        return False

    def findRedundantConnection(self, edges: List[List[int]]) -> List[int]:
        graph = {}
        for start, end in edges:
            if self.already_connected(graph, {}, start, end):
                return [start, end]
            graph.setdefault(start, set()).add(end)
            graph.setdefault(end, set()).add(start)
