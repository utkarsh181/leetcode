from collections import deque
from typing import List, Optional


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def levelOrderBottom(self, root: Optional[TreeNode]) -> List[List[int]]:
        if root is None:
            return []

        q1 = deque([root])
        q2 = deque()
        levels = [[root.val]]

        while q1:
            cur = q1.popleft()
            if cur.left is not None:
                q2.append(cur.left)
            if cur.right is not None:
                q2.append(cur.right)

            if not q1 and q2:
                levels.append([node.val for node in q2])
                q1 = q2
                q2 = deque()

        return list(reversed(levels))
