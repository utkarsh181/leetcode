from random import choice

class Solution:
    def __init__(self, nums: List[int]):
        self.index = {}
        for i, n in enumerate(nums):
            self.index[n] = self.index.get(n, []) + [i]

    def pick(self, target: int) -> int:
        return choice(self.index[target])
