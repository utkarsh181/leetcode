class MyCalendarTwo:
    def __init__(self):
        self.timeline = {}

    def book(self, start: int, end: int) -> bool:
        if start not in self.timeline:
            self.timeline[start] = 0
        if end not in self.timeline:
            self.timeline[end] = 0

        self.timeline[start] += 1
        self.timeline[end] -= 1
        rslt, cur = 0, 0
        for key in sorted(self.timeline.keys()):
            val = self.timeline[key]
            cur += val
            rslt = max(rslt, cur)
            if rslt > 2:
                self.timeline[start] -= 1
                self.timeline[end] += 1
                return False
        return True


# Your MyCalendarTwo object will be instantiated and called as such:
# obj = MyCalendarTwo()
# param_1 = obj.book(start,end)