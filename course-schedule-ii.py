from graphlib import TopologicalSorter, CycleError


class Solution:
    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        courses = {}
        for course in range(numCourses):
            courses[course] = set()
        for course, prerequisite in prerequisites:
            courses[course].add(prerequisite)
        coursesGraph = TopologicalSorter(courses)
        try:
            return list(coursesGraph.static_order())
        except CycleError:
            return []
