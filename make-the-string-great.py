class Solution:
    def makeGood(self, s: str) -> str:
        buf = []
        for c in s:
            if buf and ord(buf[-1]) ^ ord(c) == 32:
                buf.pop()
            else:
                buf.append(c)
        return ''.join(buf)
