from functools import cache


class Solution:
    @cache
    def _rec(self, idx1: int, idx2: int) -> int:
        if idx1 == len(self.s1) or idx2 == len(self.s2):
            return 0

        if self.text1[idx1] == self.text2[idx2]:
            return 1 + self._rec(idx1 + 1, idx2 + 1)

        return max(self._rec(idx1 + 1, idx2), self._rec(idx1, idx2 + 1))

    def longestCommonSubsequence(self, text1: str, text2: str) -> int:
        self.text1 = text1
        self.text2 = text2
        return self._rec(0, 0)


# class Solution:
#     def longestCommonSubsequence(self, text1: str, text2: str) -> int:
#         row = len(text1)
#         col = len(text2)
#         dp = [[0] * col for _ in range(row)]
#         for i in range(row):
#             for j in range(col):
#                 if text1[i] == text2[j]:
#                     dp[i][j] = 1 if i - 1 < 0 or j - 1 < 0 else dp[i - 1][j - 1] + 1
#                 else:
#                     left = 0 if i - 1 < 0 else dp[i - 1][j]
#                     up = 0 if j - 1 < 0 else dp[i][j - 1]
#                     dp[i][j] = max(left, up)
#         return dp[row - 1][col - 1]
