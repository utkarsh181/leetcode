class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        sums = 0
        sumsFreq = {0: 1}
        count = 0
        for n in nums:
            sums += n
            count += sumsFreq.get(sums - k, 0)
            sumsFreq[sums] = sumsFreq.get(sums, 0) + 1
        return count
        