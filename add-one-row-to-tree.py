# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def addOneRow(self, root: Optional[TreeNode], val: int, depth: int) -> Optional[TreeNode]:
        def depthNRow(root: Optional[TreeNode], depth: int, max_depth: int):
            if not root:
                return []
            if depth == max_depth:
                return [root]
            return (depthNRow(root.left, depth + 1, max_depth)
            + depthNRow(root.right, depth + 1, max_depth))

        def addRow(root: Optional[TreeNode], val: int) -> None:
            new_left = TreeNode(val, root.left)
            new_right = TreeNode(val, None, root.right)
            root.left, root.right = new_left, new_right

        if depth == 1:
            nnode = TreeNode(val, root)
            root = nnode
        else:
            for node in depthNRow(root, 1, depth - 1):
                addRow(node, val)
        return root