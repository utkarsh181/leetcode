class Solution:
    def threeSum(self, nums: list[int]) -> list[list[int]]:
        nums.sort()
        nlen = len(nums)
        rslt = []
        for i in range(nlen - 2):
            left = i + 1
            right = nlen - 1
            target = -nums[i]
            while left < right:
                tmp = nums[left] + nums[right]
                if tmp == target:
                    rslt.append((nums[i], nums[left], nums[right]))
                    left += 1
                    right -= 1
                elif tmp < target:
                    left += 1
                else:
                    right -= 1
        return map(list, set(rslt))