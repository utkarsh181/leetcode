class Solution:
    def longestSubarray(self, nums: list[int]) -> int:
        rslt = 0
        start = 0
        last_zero_in_window = 0 if nums[0] == 0 else -1

        for end, num in enumerate(nums):
            if num == 0 and last_zero_in_window == -1:
                last_zero_in_window = end
            elif num == 0:
                start = last_zero_in_window + 1
                last_zero_in_window = end
            rslt = max(rslt, end - start)

        return rslt
