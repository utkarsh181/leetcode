class Solution:
    def searchRange(self, nums: list[int], target: int) -> list[int]:
        lo = bisect.bisect_left(nums, target)
        if not nums or lo == len(nums) or target != nums[lo]:
            return [-1, -1]
        return [lo, bisect.bisect_right(nums, target) - 1]
