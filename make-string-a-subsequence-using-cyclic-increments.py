class Solution:
    def nextChar(self, ch: str) -> str:
        return "a" if ch == "z" else chr(ord(ch) + 1)

    def canMakeSubsequence(self, str1: str, str2: str) -> bool:
        idx2 = 0

        for idx1, ch in enumerate(str1):
            if idx2 == len(str2):
                return True
            if ch == str2[idx2] or self.nextChar(ch) == str2[idx2]:
                idx2 += 1

        return True if idx2 == len(str2) else False
