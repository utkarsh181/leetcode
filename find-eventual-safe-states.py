class Solution:
    def __init__(self):
        self.graph = {}
        self.discovered = {}
        self.safeNodes = {}

    @cache
    def safe(self, node: int) -> bool:
        self.discovered[node] = True
        for adjNode in self.graph[node]:
            if adjNode not in self.safeNodes:
                if not self.discovered.get(adjNode, False) and self.safe(adjNode):
                    self.safeNodes[adjNode] = True
                else:
                    return False
        self.safeNodes[node] = True
        return True

    def eventualSafeNodes(self, graph: List[List[int]]) -> List[int]:
        self.graph = graph
        for idx, nodes in enumerate(graph):
            if nodes == []:
                self.safeNodes[idx] = True

        for i in range(len(graph)):
            self.safe(i)
        return sorted(self.safeNodes.keys())
