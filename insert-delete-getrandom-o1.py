from random import randint

class RandomizedSet():
    def __init__(self):
        self.vals = []
        self.index = {}

    def insert(self, val: int) -> bool:
        if val in self.index:
            return False
        self.vals.append(val)
        self.index[val] = len(self.vals) -  1
        return True

    def remove(self, val: int) -> bool:
        if val not in self.index:
            return False
        i = self.index[val]
        lastVal = self.vals.pop()
        if i != len(self.vals):
            self.vals[i] = lastVal
            self.index[lastVal] = i
        del self.index[val]
        return True

    def getRandom(self) -> int:
        return self.vals[randint(0, len(self.vals) - 1)]
