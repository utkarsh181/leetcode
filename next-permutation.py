class Solution:
    # Algorithm: https://www.nayuki.io/page/next-lexicographical-permutation-algorithm
    def nextPermutation(self, nums: list[int]) -> None:
        n = len(nums)
        if n < 2:
            return

        reverse = False
        for k in range(n - 2, -2, -1):
            if k == -1:
                reverse = True
                break
            if nums[k] < nums[k + 1]:
                break

        if reverse:
            nums[:] = nums[::-1]
        else:
            for l in range(n - 1, k, -1):
                if nums[k] < nums[l]:
                    break

            nums[k], nums[l] = nums[l], nums[k]
            nums[k + 1 :] = nums[k + 1 :][::-1]
