class MyCalendar:
    def __init__(self):
        self.timeline = {}

    def book(self, start: int, end: int) -> bool:
        if start not in self.timeline:
            self.timeline[start] = 0
        if end not in self.timeline:
            self.timeline[end] = 0

        self.timeline[start] += 1
        self.timeline[end] -= 1
        rslt, cur = 0, 0
        for key in sorted(self.timeline.keys()):
            cur += self.timeline[key]
            rslt = max(rslt, cur)
            if rslt > 1:
                self.timeline[start] -= 1
                self.timeline[end] += 1
                return False
        return True


        


# Your MyCalendar object will be instantiated and called as such:
# obj = MyCalendar()
# param_1 = obj.book(start,end)