class Solution:
    def maximum69Number (self, num: int) -> int:
        try:
            tmp = str(num)
            i = tmp.index('6')
            return int(tmp[:i] + '9' + tmp[i+1:])
        except ValueError:
            return num
