# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    @cache
    def allPossibleFBT(self, n: int) -> List[Optional[TreeNode]]:
        if n % 2 == 0:
            return []
        elif n == 1:
            return [TreeNode(0)]
        
        rslt = []
        for i in range(1, n - 1, 2):
            for left in self.allPossibleFBT(i):
                for right in self.allPossibleFBT(n - i - 1):
                    rslt += [TreeNode(0, left, right)]
        return rslt