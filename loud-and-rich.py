class Solution:
    def __init__(self):
        self.richerGraph = {}
        self.quiet = []

    @cache
    def dfs(self, cur: int, maxLoud: int) -> int:
        for rich in self.richerGraph.get(cur, []):
            if self.quiet[maxLoud] > self.quiet[rich]:
                maxLoud = rich
            rich = self.dfs(rich, maxLoud)
            if self.quiet[maxLoud] > self.quiet[rich]:
                maxLoud = rich
        return maxLoud

    def loudAndRich(self, richer: List[List[int]], quiet: List[int]) -> List[int]:
        self.quiet = quiet
        for rich, poor in richer:
            self.richerGraph.setdefault(poor, set()).add(rich)

        rslt = []
        for i in range(len(quiet)):
            rslt.append(self.dfs(i, i))
        return rslt
