from graphlib import TopologicalSorter, CycleError


class Solution:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        courses = {}
        for course, prerequisite in prerequisites:
            courses.setdefault(course, set()).add(prerequisite)
        coursesGraph = TopologicalSorter(courses)
        try:
            coursesGraph.prepare()
            return True
        except CycleError:
            return False
