class Solution:
    def twoSum(self, nums: list[int], target: int) -> list[int]:
        buf = {}
        rslt = []
        for i, n in enumerate(nums):
            tmp = target - n
            if tmp in buf:
                rslt = [i, buf[tmp]]
                break
            buf[n] = i
        return rslt
