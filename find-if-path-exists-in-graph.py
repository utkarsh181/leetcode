from queue import Queue


class Solution:
    def initGraph(self, n: int, edges: List[List[int]]):
        graph = [[] for _ in range(n)]
        for start, end in edges:
            graph[start].append(end)
            graph[end].append(start)
        return graph

    def validPath(
        self, n: int, edges: List[List[int]], source: int, destination: int
    ) -> bool:
        if source == destination:
            return True

        graph = self.initGraph(n, edges)
        discovered = {}

        queue = Queue()
        queue.put(source)
        discovered[source] = True

        while not queue.empty():
            cur = queue.get()
            for end in graph[cur]:
                if end == destination:
                    return True
                if not discovered.get(end, False):
                    queue.put(end)
                    discovered[end] = True

        return False
