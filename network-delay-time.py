from collections import defaultdict
from heapq import heappush, heappop


class Solution:
    def getGraph(self, times: List[List[int]]) -> Dict[int, List[List[int]]]:
        graph = defaultdict(list)

        for cur, end, time in times:
            graph[cur].append((end, time))

        return graph

    def networkDelayTime(self, times: List[List[int]], n: int, k: int) -> int:
        minHeap = [(0, k)]
        graph = self.getGraph(times)
        times = {}
        known = set()

        while minHeap:
            curTime, curNode = heappop(minHeap)
            known.add(curNode)

            if curTime > times.get(curNode, float("inf")):
                continue

            for neighbour, weight in graph[curNode]:
                time = curTime + weight
                if neighbour in known:
                    continue

                if time < times.get(neighbour, float("inf")):
                    times[neighbour] = time
                    heappush(minHeap, (time, neighbour))

        rslt = times.values()
        if len(rslt) != n - 1:
            return -1
        else:
            return max(rslt)
