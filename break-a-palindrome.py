class Solution:
    def isPalindrome(self, buf: str) -> bool:
        return buf == buf[::-1]

    def breakPalindrome(self, palindrome: str) -> str:
        n = len(palindrome)
        if n == 1:
            return ''
        
        rslt = ''
        for i in range(n):
            tmp = rslt + 'a' + palindrome[i + 1:]
            if palindrome[i] == 'a' or self.isPalindrome(tmp):
                rslt += palindrome[i]
            else:
                return tmp
        return rslt[:-1] + 'b'
