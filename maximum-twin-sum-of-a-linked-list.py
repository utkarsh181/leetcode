# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def pairSum(self, head: Optional[ListNode]) -> int:
        node, llen = head, 0
        while node:
            llen += 1
            node = node.next
        
        llen //= 2
        tlist = []
        node = head
        while llen > 0:
            tlist.append(node.val)
            node = node.next
            llen -= 1
        
        msum = 0
        while node:
            tmp = tlist.pop() + node.val
            if tmp > msum:
                msum = tmp
            node = node.next
        return msum