from itertools import accumulate
from bisect import bisect
from random import random

class Solution:
    def __init__(self, w: List[int]):
        self.weight = [x / sum(w) for x in accumulate(w)]

    def pickIndex(self) -> int:
        return bisect(self.weight, random())
