class MyCalendarThree:
    def __init__(self):
        self.timeline = {}

    def book(self, start: int, end: int) -> int:
        if start not in self.timeline:
            self.timeline[start] = 0
        if end not in self.timeline:
            self.timeline[end] = 0

        self.timeline[start] += 1
        self.timeline[end] -= 1
        self.timeline = {key:self.timeline[key] for key in sorted(self.timeline.keys())}
        rslt, cur = 0, 0
        for i in self.timeline.values():
            cur += i
            rslt = max(rslt, cur)
        return rslt
