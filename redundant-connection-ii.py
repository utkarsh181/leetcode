class Solution:
    def __init__(self):
        self.edges = {}

    def isValidEdge(self, start: int, end: int) -> bool:
        def rec(start: int, end: int) -> bool:
            if start == end:
                return True

            discovered.add(start)
            for adjVert in self.edges.get(start, []):
                if adjVert not in discovered and rec(adjVert, end):
                    return True
            return False

        discovered = set()
        return rec(start, end)

    def findRedundantDirectedConnection(self, edges: List[List[int]]) -> List[int]:
        parent = {}
        twoParentNode = None

        for start, end in edges:
            self.edges.setdefault(start, []).append(end)
            parent.setdefault(end, []).append(start)
            if len(parent[end]) == 2:
                twoParentNode = end

        # Cross edge
        if not twoParentNode:
            for end, start in reversed(edges):
                if self.isValidEdge(start, end):
                    return [end, start]
        # Back edge
        elif self.isValidEdge(twoParentNode, parent[twoParentNode][0]):
            return [parent[twoParentNode][0], twoParentNode]
        # Forward edge
        else:
            return [parent[twoParentNode][1], twoParentNode]
