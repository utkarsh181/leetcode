# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def inorder(self, node: Optional[TreeNode]) -> list[int]:
        if not node:
            return []
        nums = []
        nums += self.inorder(node.left)
        nums.append(node.val)
        nums += self.inorder(node.right)
        return nums

    def findTarget(self, root: Optional[TreeNode], k: int) -> bool:
        nums = self.inorder(root)
        n = len(nums)
        left, right = 0, n - 1
        while left < right:
            tmp = nums[left] + nums[right]
            if tmp == k:
                return True
            elif tmp < k:
                left += 1
            else:
                right -= 1
        return False


    