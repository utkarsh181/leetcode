class Solution:
    def minSubArrayLen(self, target: int, nums: list[int]) -> int:
        minLen = float("inf")
        curSum = 0
        left = 0

        for right, num in enumerate(nums):
            curSum += num
            while curSum >= target:
                minLen = min(minLen, right - left + 1)
                curSum -= nums[left]
                left += 1

        return 0 if minLen == float("inf") else minLen
