from random import randrange


class Solution:
    def __init__(self, n: int, blacklist: List[int]):
        blacklist = set(blacklist)
        last = n - 1
        self.blacklist = {}
        self.size = n - len(blacklist)
        for n in blacklist:
            while last in blacklist:
                last -= 1
            self.blacklist[n] = last
            last -= 1
        
    def pick(self) -> int:
        n = randrange(self.size)
        if n not in self.blacklist:
            return n
        else:
            return self.blacklist[n]
