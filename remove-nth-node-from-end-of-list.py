# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        def length(head: ListNode) -> int:
            if not head:
                return 0
            count = 0
            while head:
                count += 1
                head = head.next
            return count
        
        def removeNth(head: ListNode, n: int) -> ListNode:
            if not head:
                return
            if n == 0:
                return head.next
            prev = cur = head
            while n > 0:
                prev = cur
                cur = cur.next
                n -= 1
            prev.next = cur.next
            return head
            
        return removeNth(head, length(head) - n)