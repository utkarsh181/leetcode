class Solution:
    def removeDuplicates(self, s: str) -> str:
        rslt = []
        for c in s:
            if rslt and rslt[-1] == c:
                rslt.pop()
            else:
                rslt.append(c)
        return ''.join(rslt)
