class Solution:
    def __init__(self):
        self.rslt = []
        self.nums = []

    def _backtrack(self, start: int, subset: list[int]):
        if start > len(self.nums):
            return

        self.rslt.append(subset)
        for idx, num in enumerate(self.nums[start:]):
            abs_idx = start + idx
            if idx > 0 and self.nums[abs_idx - 1] == num:
                continue
            self._backtrack(abs_idx + 1, subset + [num])

    def subsetsWithDup(self, nums: list[int]) -> list[list[int]]:
        self.nums = sorted(nums)
        self._backtrack(0, [])
        return self.rslt
