bool isIsomorphic(char * s, char * t) {
  int i;
  char t1[128] = {0}, t2[128] = {0};

  for (i = 0; s[i] && t[i]; i++) {
    if (!t1[s[i]]) {
      t1[s[i]] = t[i];
    }
    else if (t1[s[i]] != t[i]) {
      return false;
    }

    if (!t2[t[i]]) {
      t2[t[i]] = s[i];
    }
    else if (t2[t[i]] != s[i]) {
      return false;
    }
  }
  return true;
}