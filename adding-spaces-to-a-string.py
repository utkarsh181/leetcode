class Solution:
    def addSpaces(self, s: str, spaces: list[int]) -> str:
        rslt = ""
        space_hash = set(spaces)

        if 0 in space_hash:
            rslt += " "

        for idx, ch in enumerate(s):
            rslt += ch
            if idx + 1 in space_hash:
                rslt += " "

        return rslt
