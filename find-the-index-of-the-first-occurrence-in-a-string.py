# Rabin-Karp algorithm for efficient string matching.

class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        needleHash = hash(needle)
        for i in range(len(haystack) - len(needle) + 1):
            tmpStr = haystack[i : i + len(needle)]
            tmpHash = hash(tmpStr)
            if tmpHash == needleHash and tmpStr == needle:
                return i
        return -1
