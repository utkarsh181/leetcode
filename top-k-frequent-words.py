class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:
        rslt = []
        for word, count in collections.Counter(sorted(words)).most_common(k):
            rslt.append(word)
        return rslt