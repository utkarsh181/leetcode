class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        tmp = {}
        for idx, num in enumerate(nums):
            if num not in tmp:
                tmp[num] = [idx]
            else:
                tmp[num].append(idx)
        
        for num in tmp:
            if len(tmp[num]) > 1:
                prev = tmp[num][0]
                for cur in tmp[num][1:]:
                    if cur - prev <= k:
                        return True
                    prev = cur

        return False