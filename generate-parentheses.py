class Solution:
    def __init__(self):
        self._rslt = []

    def generateParenthesis(self, n: int) -> List[str]:
        def generate(p: str, left: int, right: int) -> None:
            if left:
                generate(p + '(', left - 1, right)
            if right > left:
                generate(p + ')', left, right - 1)
            if not right:
                self._rslt.append(p)

        generate('', n, n)
        return self._rslt
