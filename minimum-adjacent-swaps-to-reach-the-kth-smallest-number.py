class Solution:
    # Algorithm: https://www.nayuki.io/page/next-lexicographical-permutation-algorithm
    def nextPermutation(self, nums: list[int]) -> None:
        n = len(nums)
        if n < 2:
            return

        reverse = False
        for k in range(n - 2, -2, -1):
            if k == -1:
                reverse = True
                break
            if nums[k] < nums[k + 1]:
                break

        if reverse:
            nums[:] = nums[::-1]
        else:
            for l in range(n - 1, k, -1):
                if nums[k] < nums[l]:
                    break

            nums[k], nums[l] = nums[l], nums[k]
            nums[k + 1 :] = nums[k + 1 :][::-1]

    def getMinAdjSwaps(self, src: list[int], dest: list[int]) -> int:
        count = 0
        size = len(src)

        for i in range(size):
            if src[i] == dest[i]:
                continue

            for j in range(i + 1, size):
                if src[j] == dest[i]:
                    break

            for k in range(j, i, -1):
                count += 1
                src[k - 1], src[k] = src[k], src[k - 1]

        return count

    def getMinSwaps(self, num: str, k: int) -> int:
        src = [int(c) for c in num]
        dest = src[:]

        for _ in range(k):
            self.nextPermutation(dest)

        return self.getMinAdjSwaps(src, dest)
