class Solution:
    def iter(self, n: str) -> list:
        rslt = []
        prev = n[0]
        count = 1
        for cur in n[1:]:
            if prev != cur:
                rslt.append([prev, str(count)])
                count = 0
            prev = cur
            count += 1
        rslt.append([prev, str(count)])
        return rslt

    def combine(self, counts: list) -> str:
        rslt = ''
        for ch, count in counts:
            rslt += count + ch
        return rslt

    def countAndSay(self, n: int) -> str:
        if n == 1:
            return '1'
        
        rslt = '1'
        while n > 1:
            rslt = self.combine(self.iter(rslt))
            n -= 1
        return rslt
