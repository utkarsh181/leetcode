from heapq import heapify, heappush, heappop

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def mergeKLists(self, lists: List[Optional[ListNode]]) -> Optional[ListNode]:
        heap = [(l.val, i, l) for i, l in enumerate(lists) if l]
        heapify(heap);
        dummy = cur = ListNode()
        while heap:
            tmp = heappop(heap)
            cur.next = tmp[2]
            if tmp[2].next:
                heappush(heap, (tmp[2].next.val, tmp[1], tmp[2].next))
            cur = cur.next
        return dummy.next