class Solution:
    def __init__(self):
        self.rslt = []
        self.candidates = []

    def _backtrack(self, start: int, target: int, combination: list[int]) -> None:
        if target < 0:
            return
        if target == 0:
            self.rslt.append(combination)

        for idx, num in enumerate(self.candidates[start:]):
            self._backtrack(start + idx, target - num, combination + [num])

    def combinationSum(self, candidates: list[int], target: int) -> list[list[int]]:
        self.candidates = candidates
        self._backtrack(0, target, [])
        return self.rslt
