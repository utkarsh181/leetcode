class Solution:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        heap = [(sqrt(p[0] ** 2 + p[1] ** 2), p[0], p[1]) for p in points]
        rslt = []
        heapify(heap)
        while heap and k > 0:
            h = heappop(heap)
            rslt.append([h[1], h[2]])
            k -= 1
        return rslt
